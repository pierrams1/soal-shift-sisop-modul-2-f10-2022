# soal-shift-sisop-modul-2-F10-2022 

NAMA - NRP
<br>
Pierra Muhammad Shobr - 5025201062<br>
Andi Muhammad Rafli.  - 5025201089<br>
Ghazi Buana Dewa - 5025201074<br>
<br>

# Soal 2

<p>Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.
</p>
<p>
a. Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.
</p>
<p>
b. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.
</p>
<p>
c. Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.
</p>
<p>
d. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto)
</p>
<p>
e. Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh dibawah ini.
</p>
### A

#### Ekstract dan create folder untuk menampung hasil ekstract file `drakor`
```
void create_folder(pid_t child_id){
    int status;
    
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "/oki/shift2/drakor", NULL};
        execv("/bin/mkdir", argv);
    }else{
        while((wait(&status)) > 0);
        char *argv[] = {"unzip", "/oki/sisop/modul2/drakor.zip", NULL};
        execv("/bin/unzip", argv);
    }    
}
```

Saat parent proses jalan, program akan mengektract file `drakor.zip` dengan syntax `unzip` yang dieksekusi di terminal dengan `execv`. Selanjutnya child proses akan berjalan dan membuat folder `drakor`.

#### Menghapus folder yang tidak dibutuhkan

```
void delete_folder(pid_t child_id){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);

    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    char current[PATH_MAX];
    int status;

    // get current location
    strcpy(current, cwd);

    dir = opendir(current);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            // Construct new path from our base path
            strcpy(path, current);
            strcat(path, "/");
            strcat(path, dp->d_name);

            if(strchr(dp->d_name, '.') == 0 && strcmp(dp->d_name, "soal2") != 0){            
                if(child_id == 0){
                    printf("%s\n", dp->d_name);

                    char *argv[] = {"rm", "-rf", dp->d_name, NULL};
                    execv("/bin/rm", argv);
                }
                while((wait(&status)) > 0);
                child_id = fork();
            }
        }
    }
}
```
Dalam membaca file-file yang terdapat dalam directory, kami menggunakan library `dirent.h`. Program akan mendapatkan folder location dengan syntax `strcpy(current, cwd)` dimana lokasi file ditampung dalam variabel `current`. Selanjutnya program akan mengecek nama-nama file dimana nantinya jika terdapat file tanpa "." yang menandakan file folder, maka program akan mengeksekusi sytax `rm -rf` yang akan menghapus folder tidak penting.

### B
#### Membuat Folder Genre
```
void create_genre(pid_t child_id){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);
    char current[PATH_MAX];

    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    char myDir[PATH_MAX];
    int status;

    strcpy(current, cwd);

    dir = opendir(current);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){

            // strcpy(path, current);
            // strcat(path, "/");
            // strcat(path, dp->d_name);

            if(child_id == 0){
                char fileName[PATH_MAX];
                strcpy(fileName, dp->d_name);

                delete_substr(fileName, ".png");

                char name[PATH_MAX];
                char *split;
                split = strtok(fileName, "_;");

                int flag = 0;
                while(split != NULL){
                    if(flag == 2){
                        strcpy(name, split);
                        int len = strlen(name);
                        // if(len <= 8){
                            strcpy(myDir, "/oki/shift2/drakor/");
                            strcat(myDir, name);
                            printf("%s --> %s\n", myDir, name);
                            
                            char *argv[] = {"mkdir", "-p", myDir, NULL};
                            execv("/bin/mkdir", argv);
                        // }
                    }
                    split = strtok(NULL, "_;");
                    flag++;
                }
            }
            while((wait(&status)) > 0);
            child_id = fork();
        }
    }

}
```
Untuk membaca nama file program menggunakan library `dirent.h`. Dalam mencari genre dari drakor, program menampung nama file, menghapus karakter `.png`, dan menjalankan `strtok` pada karater `;` yang memberikan sekat antar identitas dari drakor. Untuk mendapatkan genre film, program melakukan while loop dengan penanda variabel `flag` yang akan bertambah setiap kali dijalankan `strtok`. Karena genre film terletak setelah `;` kedua maka saat `flag` = 2, program akan membuat directory di folder `drakor` dengan `mkdir` yang dijalankan diterminal melalui `execv`.

### C & D
#### Membuat duplikat file
```
void duplicate_image(){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);
    char current[PATH_MAX];

    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    int status;

    strcpy(current, cwd);
    dir = opendir(current);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && 
            strcmp(dp->d_name, "soal2") != 0 && strcmp(dp->d_name, "soal2.c") != 0){

            char fileName[NAME_MAX];
            strcpy(fileName, dp->d_name);
            delete_substr(fileName, ".png");

            strcpy(path, current);
            strcat(path, "/");

            char title[NAME_MAX];
            char name[NAME_MAX];
            char year[NAME_MAX];

            if(strchr(fileName, '_') != 0){
                char *token;
                token = strtok(fileName, "_;");

                int flag = 0;
                while(token != NULL){
                    if(flag == 0){
                        strcpy(title, token);
                    }

                    if(flag == 1){
                        strcpy(year, token);
                    }

                    if(flag == 2){
                        strcpy(name, token);
                    }

                    token = strtok(NULL, "_;");
                    flag++;

                }else{continue;}
```
Sama seperti fungsi sebelumnya, dalam fungsi ini akan didekalrasikan variabel dari library `dirent.h`. Masing-masing identitas file ditampung dalam variabel char berbeda yakni `title`, `name`, dan `year` jika terdapat dua drakor dalam 1 file

```
 strcat(path, dp->d_name);

            char dest[PATH_MAX];
            strcpy(dest, current);
            strcat(dest, "/");
            strcat(dest, title);
            strcat(dest, ";");
            strcat(dest, year);
            strcat(dest, ";");
            strcat(dest, name);
            strcat(dest, ".png");


            printf("Duplicate = %s --> %s\n", path, dest);
            pid_t ch = fork();
            int status;

            if (ch < 0) exit(EXIT_FAILURE);

            if(ch == 0){
                char *argv[] = {"cp", path, dest, NULL};
                execv("/bin/cp", argv);
            }else{
                while((wait(&status)) > 0);
                ch = fork();
            }
        }
    }

    closedir(dir);
}
```

Selanjutnya program menampung nama file copy dalam variabel `dest` dengan memasukan semua identitas dari drakor. Setelah selesai, program akan mengcopy foto dan menamainya sesuai dengan identitas drakor apa saja yang terdapat dalam file.

#### Menempatkan file duplikat sesuai genre
```
void create_data(pid_t child_id){
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);
    char current[PATH_MAX];

    DIR *dir;
    struct dirent *dp;
    char path[PATH_MAX];
    char myPath[PATH_MAX];
    int status;
    int status2;

    strcpy(current, cwd);

    dir = opendir(current);
    strcpy(path, "/oki/shift2/drakor/");
    strcpy(myPath, current);
    strcat(myPath, "/");

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 
            && strcmp(dp->d_name, "soal2") != 0 && strcmp(dp->d_name, "soal2.c") != 0) {

            char fileName[PATH_MAX];
            strcpy(fileName, dp->d_name);

            delete_substr(fileName, ".png");

            char name[PATH_MAX];
            char title[PATH_MAX];
            char year[PATH_MAX];
            char *split;
            split = strtok(fileName, "_;");

            int flag = 0;
            while(split != NULL){

                if(flag == 0 | flag == 3){
                    strcpy(title, split);
                }

                if(flag == 2 | flag == 5){
                    // printf("CREATE FOLDER\n");
                    strcpy(name, split);
                }

                if(flag == 1 | flag == 4){
                    strcpy(year, split);
                }

                // printf("%s -> %s -> %s\n", name, title, year);
                if(strchr(dp->d_name, '/') == 0){
                    if(( strlen(title) > 0 & strlen(name) > 0 & strlen(year) > 0 )){
                        // C
                        copy_image(dp->d_name, title, name, year, myPath);
                        // add_data(title, name, year);
                    }
                }
                
                split = strtok(NULL, "_;");
                flag++;
            }
        }
    }

    // closedir(dir);
}
```
Pada fungsi `create_data()` identitas dari film akan ditampung dalam beberapa variabel char yakni `name`, `title`, dan `year` dengan membagi string filename melalui syntax `strtok`. Data-data terkait file selanjutnya akan diproses dalam fungsi `copy_image`.

```
void copy_image(char *fileName, char title[NAME_MAX], char name[NAME_MAX], char year[NAME_MAX], char path[]){

    char from[PATH_MAX];
    strcpy(from, path);
    strcat(from, fileName);

    delete_substr(fileName, ".png");
    char *token;
    token = strtok(fileName, "_;");

    int flag = 0;
    while(token != NULL){
        if(flag == 0 | flag == 3){
            // if(strcmp(title, ))
            strcpy(title, token);
            // printf("Title %s ", title);
        }

        if(flag == 1 | flag == 4){
            strcpy(year, token);
            // printf("Name: %s\n", name);
        }

        if(flag == 2 | flag == 5){
            strcpy(name, token);
            // printf("Name: %s\n", name);
        }
        

        flag++;
        token = strtok(NULL, "_;");
    }
    
    char dest[PATH_MAX];
    strcpy(dest, "/oki/shift2/drakor/");
    strcat(dest, name);
    strcat(dest, "/");
    strcat(dest, title);
    strcat(dest, ".png");
    
    // printf("Copy = %s --> %s\n", from, dest);

    pid_t ch = fork();
    int status;

    if(ch < 0) exit(EXIT_FAILURE);

    if(ch == 0){
        // printf("%s --> %s\n", from, dest);
        char *argv[] = {"cp", from, dest, NULL};
        execv("/bin/cp", argv);
    }else{
        while((wait(&status)) > 0);
        add_data(title, name, year);
        return;
    }

}
```
Fungsi akan memastikan ulang identitas file drakor dengan menjalankan `strtok` dan `while loop` nama dari file drakor. selanjutnya file path dari drakor akan ditampung dengan `strcat` ke dalam variabel dest sesuai dengan genre dari file drakor. Dengan parent proses, program memanggil fungsi `add_data()` yang akan menampung data dari drakor. Selanjutnya dalam child proses program akan mengcopy file drakor (variabel `from`) sesuai dengan genre yang telah ditampung dalam variabel `dest` dan mengeksekusi command `cp` dengan syntax `execv`.

### E
```
void add_data(char title[NAME_MAX], char name[NAME_MAX], char year[NAME_MAX]){
    FILE *fp;
    char addPath[PATH_MAX];
    char cName[NAME_MAX];
    char checkName[NAME_MAX];
    char checkTitle[NAME_MAX];
    char line[100000];


    strcpy(addPath, "/oki/shift2/drakor/");
    strcat(addPath, name);
    strcat(addPath, "/data.txt");

    fp = fopen(addPath, "a+");
    
    strcpy(checkName, "kategori: ");
    strcat(checkName, name);
    fscanf(fp, "%[^\n]", cName);

    if (strcmp(cName, checkName) != 0){
        fprintf(fp, "kategori: %s\n\n", name);
    }

    strcpy(checkTitle, title);

    bool status = false;
    while(fgets(line, sizeof(line), fp)){

        if(strstr(line, checkTitle) != 0){
            printf("same\n");
            printf("%s\n", line);
            status = true;
            break;
        }
        // printf("%s\n", line);
        // printf("-----------------------------------\n");
        
    }

    if(!status){
        // printf("TULIS\n");
        fprintf(fp, "nama: %s\nrilis: %s\n\n", title, year);
    }

    fclose(fp);
}
```
Sebelum menulis identitas drakor, fungsi akan mengecek apakah kategori dari drakor sudah di tulis dalam file `txt` ini. Fungsi juga akan mengecek jika judul drakor sudah ditulis. Jika judul drakor sudah ada, maka variabel `status` menjadi `false` dan fungsi berakhir. Jika judul drakor belum ada, program akan menulis judul drakor dan tahun rilis dengan syntax `fprintf`.

##### Dokumentasi Jalannya Program
![2. Dokumentasi Program Jalan](image/2. Dokumentasi Program Jalan.PNG)

##### Folder kategori film
![2. Directory kategori film](image/2. Directory kategori film.PNG)

##### list film dalam kategori (ambil salah satu yaitu action)
![2. Isi film dalam kategori ](image/2. Isi film dalam kategori.PNG)

##### File data.txt (ambil contoh satu dari action)
![2. isi file data ](image/2. isi file data.PNG)

# Soal 3
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang
    a.Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.
    b.Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.
    c.Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.
    d.Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.
    e.Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

catatan:
-Tidak boleh memakai system().
-Tidak boleh memakai function C mkdir() ataupun rename().
-Gunakan exec dan fork
-Direktori “.” dan “..” tidak termasuk

### A
```
void create_dir(){
    pid_t ch = fork();
    int status;

    if(ch == 0){
        char *argv[] = {"mkdir", "-p", "/home/rendi/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    }else{
        while((wait(&status)) > 0);
        sleep(3);
        char *argv[] = {"mkdir", "-p", "/home/rendi/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    }
}
```
Membuat directory darat dan air pada /home/rendi/modul2/.Pembuatan directory dimualai dari directory darat terlebih dahulu kemudian tunggu 3 detik setelah itu membuat directory air.Untuk jeda 3 detik digunakan "sleep(3)"

### B
```
void unzip(){

    int status;

    if(fork() == 0){
        char *argv[] = {"unzip", "/home/rendi/sisop/modul2/animal.zip", "-d", "/home/rendi/modul2/", NULL};
        execv("/bin/unzip", argv);
    }
    while((wait(&status)) > 0 );
}
```
File zip berada pada directory "/home/rendi/sisop/modul2/animal.zip" kemudian diunzip ke dalam directory "/home/rendi/modul2/" dengan sebuah perintah "-d" yaitu tempat kemana file akan diunzip.
### C
```
void delete_substr(char *str, char *substr){
    char *comp;
    int png = strlen(substr);
    while((comp = strstr(str, substr))){
        *comp = '\0';
        strcat(str, comp+png);
    }
}
void move_animals(){
    DIR *dir;
    struct dirent *dp;
    
    char from[PATH_MAX];
    char dest[PATH_MAX];

    strcpy(from, "/home/rendi/modul2/animal/");
    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
            
            char fileName[NAME_MAX];
            strcpy(fileName, dp->d_name);
            delete_substr(fileName, ".jpg");

            strcpy(from, "/home/rendi/modul2/animal/");
            strcat(from, dp->d_name);
            strcpy(dest, "/home/rendi/modul2/");

            pid_t child = fork();
            int status;
            
            char *token;
            token = strtok(fileName, "_");
            int flag = 0;
            while(token != NULL){

                if(flag == 1){
                    if(strcmp(token, "air") == 0){
                        // printf("AIR: %s --> %s\n\n", from, dest);
                        strcat(dest, "air");
                        // printf("air %s --> %s\n", from, dest);

                        if(child == 0){
                            char *argv[] = {"mv", from, dest, NULL};
                            execv("/bin/mv", argv);
                        }
                        while((wait(&status)) > 0);
                    }

                    if(strcmp(token, "darat") == 0 || strcmp(token, "bird") == 0){
                        // printf("DARAT: %s --> %s\n\n", from, dest);
                        strcat(dest, "darat");
                        // printf("darat %s --> %s\n", from, dest);
                        if(child == 0){
                            char *argv[] = {"mv", from, dest, NULL};
                            execv("/bin/mv", argv);
                        }
                        while((wait(&status)) > 0);
                    }
                }

                token = strtok(NULL, "_");
                flag++;
            }

            if(child == 0){
                char *argv[] = {"rm", "-rf", from, NULL};
                execv("/bin/rm", argv);
            }
            while((wait(&status)) > 0);
        }
    }
}
```
Langkah awalnya yaitu menuju directory "/home/rendi/modul2/animal/" kemudian abaikan file "." dan ".." kemudian simpan nama file lalu sesuaikan dengan kriteria nama file yang dimaksud jika nama file ada kata darat maka akan dipindah ke directory "/home/rendi/modul2/animal/darat" jika nama file ada kata air maka file akan dipindahkan ke directory "/home/rendi/modul2/animal/air" dan jika nama file tidak ada kata darat ataupun air maka file tersebut akan dihapus.
### D
```
void delete_bird(){
    DIR *dir;
    struct dirent *dp;
    
    char path[PATH_MAX];

    strcpy(path, "/home/rendi/modul2/darat/");
    dir = opendir(path);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
            
            strcpy(path, "/home/rendi/modul2/darat/");
            strcat(path, dp->d_name);
            pid_t child = fork();
            int status;

            if(strstr(dp->d_name, "bird") != 0){
                if(child == 0){
                    char *argv[] = {"rm", "-rf", path, NULL};
                    execv("/bin/rm", argv);
                }
                while((wait(&status)) > 0);
            }
        }
    }
}
```
Karena file dengan nama bird ada di folder darat maka buka directory "/home/rendi/modul2/darat/" lalu selama filenya masih ada isinya cek nama file ("." dan ".." diabaikan) jika nama file terdapat kata "bird" maka file akan dihapus.
### E
```
void create_list(){
    FILE *fp;
    DIR *dir;
    struct stat info;
    struct stat fs;
    struct dirent *dp;
    int r;
    int p;

    char path[PATH_MAX];
    char permission[NAME_MAX];
    char list[NAME_MAX];

    strcpy(path, "/home/rendi/modul2/air/");
    strcpy(list, path);
    strcat(list, "list.txt");
    fp = fopen(list, "a");
    dir = opendir(path);

    // permission
    p = stat(path, &fs);
    if (p == -1){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    // owner
    r = stat(path, &info);
    if( r==-1 ){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
            
            strcat(path, dp->d_name);

            char owner[NAME_MAX];
            char p_r[NAME_MAX];
            char p_w[NAME_MAX];
            char p_x[NAME_MAX];
            
            // owner
            struct passwd *pw = getpwuid(info.st_uid);
            if (pw != 0) strcpy(owner, pw->pw_name); //puts(pw->pw_name);

            // permission
            if (fs.st_mode & S_IRUSR) strcpy(p_r, "r");
            if (fs.st_mode & S_IWUSR) strcpy(p_w, "w");
            if (fs.st_mode & S_IXUSR) strcpy(p_x, "x");

            if(strstr(dp->d_name, "list") == 0){
                fprintf(fp, "%s_%s%s%s_%s\n", owner, p_r, p_w, p_x, dp->d_name);
            }
        }
    }

    fclose(fp);
    return;
}
```
Pertama buka diectory "/home/rendi/modul2/air/" lalu buat file "list.txt" lalu pada directory "/home/rendi/modul2/air/" setiap file akan dicek UID dan file permission lalu akan dituliskan ke dalam file "list.txt" dengan format penamaan yang sudah ditentukan.Untuk menadapatkan UID dilakukan dengan cara berikut
```
 // owner
            struct passwd *pw = getpwuid(info.st_uid);
            if (pw != 0) strcpy(owner, pw->pw_name); //puts(pw->pw_name);
```
Untuk mendapatkan file permission menggunakan cara berikut
```
 // permission
            if (fs.st_mode & S_IRUSR) strcpy(p_r, "r");
            if (fs.st_mode & S_IWUSR) strcpy(p_w, "w");
            if (fs.st_mode & S_IXUSR) strcpy(p_x, "x");
```
Kemudian akan ditulis di "list.txt" dengan cara berikut
```
if(strstr(dp->d_name, "list") == 0){
                fprintf(fp, "%s_%s%s%s_%s\n", owner, p_r, p_w, p_x, dp->d_name);
           }
```

##### Dokumentasi jalannya program
![3. Dokumentasi Program Jalan](image/3. Dokumentasi Program Jalan.PNG)

##### directory darat dan air
![3. Directory laut dan darat](image/3. Directory laut dan darat.PNG)

##### list file di directory darat
![3. Directory darat](image/3. Directory darat.PNG)

##### list file di directory air
![3. Directory laut](image/3. Directory laut.PNG)

##### isi dari file.txt di directory air
![3. File list laut](image/3. File list laut.PNG)
